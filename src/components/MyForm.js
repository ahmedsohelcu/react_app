import React, {useState} from 'react';

import Example from './Counter';

const MyForm = () =>{
    const [state,setState] = useState({
        task:''
    });


    const [todoList,setlists] = useState(
        [
            {
                task: 'First Task'
            },
            {
                task: '2nd Task'
            },
            {
                task: '3rd Task'
            },
            {
                task: '4th Task'
            }
        ]
    );


    const storeForm =(e) =>{
        e.preventDefault();
        let new_todo = { task: state.task };

        setlists(todoList.concat([
            new_todo
        ]))

    };


    const handleForm = (e)=>{
        let name = e.target.name;
        // let value = e.target.value;
        const value = e.target.value;
        console.log(value);

        setState({
            ...state,
            [name]:value
        });

    };


    const editTask = (e, ind)=>{
        e.preventDefault();
        console.log(ind)
    };



return(
        <div>
            <hr/>
            <h2>From My form </h2>

          <div className="container">
              <div className="row">
                  <div className="col-md-6">

                    <Example/> <br/> <br/>

                      <form onSubmit={storeForm}>
                          <table className={'table table-striped'}>
                              <tbody>
                                  <tr>
                                      <td>Task Name :</td>
                                      <td>
                                          <input type="text" name={'task'} value={state.phone} onChange={handleForm} className={'form-control'}/>
                                      </td>
                                  </tr>

                                    <tr>
                                        <td className='pull-right'>
                                            <input type="submit" className={'btn btn-info'} value="Add"/> &nbsp;
                                        </td>
                                    </tr>

                              </tbody>
                          </table>
                      </form>


                      <table className={'table table-bordered'}>
                          <thead>
                              <tr>
                                  <th>Serial</th>
                                  <th>Task</th>
                                  <th>Action</th>
                              </tr>
                          </thead>

                          <tbody>
                              {
                                  todoList.map((todo,id) =>{
                                      return (
                                          <tr>
                                              <td>{++id}  </td>

                                              <td key={id}>{todo.task}  </td>

                                              <td>
                                                  <button className='btn btn-sm btn-info' onClick={
                                                      (e) => editTask(e, id)
                                                  }>
                                                     Edit id-{id}
                                                  </button>
                                               &nbsp;

                                                  {/*<button onClick={delTask(id)} className='btn btn-sm btn-secondary'>*/}
                                                  {/*   Delete id-{id}*/}
                                                  {/*</button>*/}
                                              </td>
                                          </tr>
                                      );
                                  })
                              }
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
    );
};

export default MyForm;

