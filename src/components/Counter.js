import React, { useState } from 'react';

const Example = () =>{

    const [count, setCount] = useState(0);

    return (
        <div className='card card-body'>
            <strong> You clicked {count} times</strong>

            <button onClick={() => setCount(count + 1)}>
                Click me
            </button>
        </div>
    );
};

export default Example;