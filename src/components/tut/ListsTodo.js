import React,{useState} from 'react';

const ListsTodo = (props)=>{

    return(
        <div>
            <p>From lists Todo form </p>
            <table className='table table-bordered table-striped'>
                <thead className={'bg-dark text-light'}>
                    <tr className={'text-center'}>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody className={'text-left'}>
                {
                    props.todoLists.map((todo,index)=>{
                       return(
                           <tr key={index} >
                               <td>{todo.id}</td>
                               <td>{todo.name}</td>
                               <td>{todo.email}</td>
                               <td>
                                   <button onClick={ e => props.edit(todo.id,e) } className="btn btn-info btn-sm">Edit</button>&nbsp;
                                   <button onClick={ e => props.deleteTodo(todo.id,e) } className="btn btn-danger btn-sm">Delte</button>&nbsp;
                               </td>
                           </tr>
                       )
                    })
                }
                </tbody>
            </table>
        </div>
    )
};

export default ListsTodo;