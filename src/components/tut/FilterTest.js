import React from 'react'

const FilterTest =  (props) =>{
    return(
      <div>
          <div className='container row'>
              <div className='col-md-4'>
                  <h2>All Persons <br/> who's id > 10 </h2><hr/>
                  {
                      props.data.filter( person => person.id > 10).map(person =>{
                          return(
                              <li className='text-justify'>{person.id}-{person.name}</li>
                          )
                      })
                  }
              </div>


              <div className="col-md-4">
                  <h2>Person id's less then 10 </h2><hr/>
                  {
                      props.data.filter((person,index)=>person.id < 10).map((person)=>{
                          return(
                              <li className='text-justify'>{person.id} - {person.name}</li>
                          )
                      })
                  }
              </div>


              <div className="col-md-4">
                  <h2>to uppercase..</h2><hr/>
                  {
                      props.data.filter((person,inx)=>person.id < 10).map((person,index)=>{
                          return(
                              <li className='text-justify'>{person.id} - {person.name.toUpperCase()}</li>
                          )
                      })
                  }
              </div>
          </div>
      </div>
    );
};

export default FilterTest;

