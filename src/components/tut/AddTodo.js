import React,{useState} from 'react';

const AddTodo = (props)=>{

    return(
        <div className={'card card-body'}>
            <form onSubmit={props.formSubmit}>
                <div  className="text-left form-group">
                    <label>Name</label>
                    <input type="text" name={'name'} value={props.todo.name} onChange={props.inputHandler} className="form-control" placeholder="Enter name"/>
                </div>

                <div className="text-left form-group">
                    <label>Email</label>
                    <input type="email" name={'email'} value={props.todo.email} onChange={props.inputHandler} className="form-control" placeholder="Password"/>
                </div>

                <div className="text-left form-group">
                    <button type="submit" className="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    )
};

export default AddTodo;


