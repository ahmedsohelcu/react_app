import React from 'react';

import Person from './Person';
import Test from '../Test'

const Persons = (props) =>{
    // const persons = [
    //     {
    //         "id": 0,
    //         "name": "Vernon Dunham",
    //         "company": "Qualcore",
    //         "email": "vernon.dunham@qualcore.com"
    //     },
    //     {
    //         "id": 1,
    //         "name": "Dori Neal",
    //         "company": "Sunopia",
    //         "email": "dori.neal@sunopia.com"
    //     },
    //     {
    //         "id": 2,
    //         "name": "Rico Muldoon",
    //         "company": "Airconix",
    //         "email": "rico.muldoon@airconix.com"
    //     },
    //     {
    //         "id": 3,
    //         "name": "Jason Neal",
    //         "company": "Qualcore",
    //         "email": "jason.neal@qualcore.com"
    //     },
    //     {
    //         "id": 4,
    //         "name": "Rico Pettey",
    //         "company": "Thermolock",
    //         "email": "rico.pettey@thermolock.com"
    //     },
    //     {
    //         "id": 5,
    //         "name": "Raymond Seabury",
    //         "company": "Airconix",
    //         "email": "raymond.seabury@airconix.com"
    //     }, {
    //         "id": 6,
    //         "name": "Dori Pettey",
    //         "company": "Hivemind",
    //         "email": "dori.pettey@hivemind.com"
    //     }, {
    //         "id": 7,
    //         "name": "Vernon Neal",
    //         "company": "Qualcore",
    //         "email": "vernon.neal@qualcore.com"
    //     }, {
    //         "id": 8,
    //         "name": "Jason Neal",
    //         "company": "Qualcore",
    //         "email": "jason.neal@qualcore.com"
    //     }, {
    //         "id": 9,
    //         "name": "Vernon Muldoon",
    //         "company": "Airconix",
    //         "email": "vernon.muldoon@airconix.com"
    //     },
    //     {
    //         "id": 10,
    //         "name": "Vernon Seabury",
    //         "company": "Hivemind",
    //         "email": "vernon.seabury@hivemind.com"
    //     },
    //     {
    //         "id": 11,
    //         "name": "Dori Neal",
    //         "company": "Airconix",
    //         "email": "dori.neal@airconix.com"
    //     },
    //     {
    //         "id": 12,
    //         "name": "Raymond Pettey",
    //         "company": "Airconix",
    //         "email": "raymond.pettey@airconix.com"
    //     },
    //     {
    //         "id": 13,
    //         "name": "Rico Muldoon",
    //         "company": "Qualcore",
    //         "email": "rico.muldoon@qualcore.com"
    //     },
    //     {
    //         "id": 14,
    //         "name": "Vernon Seabury",
    //         "company": "Sunopia",
    //         "email": "vernon.seabury@sunopia.com"
    //     },
    //     {
    //         "id": 15,
    //         "name": "Rico Pettey",
    //         "company": "Airconix",
    //         "email": "rico.pettey@airconix.com"
    //     },
    //     {
    //         "id": 16,
    //         "name": "Jason Dunham",
    //         "company": "Sunopia",
    //         "email": "jason.dunham@sunopia.com"
    //     }, {
    //         "id": 17,
    //         "name": "Vernon Neal",
    //         "company": "Qualcore",
    //         "email": "vernon.neal@qualcore.com"
    //     },
    //     {
    //         "id": 18,
    //         "name": "Jason Pettey",
    //         "company": "Thermolock",
    //         "email": "jason.pettey@thermolock.com"
    //     },
    //     {
    //         "id": 19,
    //         "name": "Vernon Dunham",
    //         "company": "Thermolock",
    //         "email": "vernon.dunham@thermolock.com"
    //     }
    // ];

    // persons.map( (person) => {
    //     return(
    //         <div>
    //             <h3>{persons[0].id}</h3>
    //         </div>
    //     )
    // });


    // calling from app.js file
    /*<Person person={person} />*/

/*
    <div className="col-md-3">
        <div className="card">
            <ul className="list-group list-group-flush">
                <li className="list-group-item">{person.id}</li>
                <li className="list-group-item">{person.name}</li>
                <li className="list-group-item">{person.company}</li>
            </ul>
        </div>
    </div>
*/

    return(
        props.persons.map( (person,index) => {
            return(
                <Person key="index" pr={person}/>
            )
        })
    );

};

export default Persons;




























