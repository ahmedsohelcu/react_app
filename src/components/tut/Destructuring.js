import React from 'react';


const Destructuring = () =>{


    //1. destructuring from object .....
    const person =
        {
            name: 'Bangladesh',
            email: 'email@gmail.com',
            phone: '018554545454'
        };

    const {name,email,phone} = person;




    //2. destructuring from array .....






    //
    //
    //
    // /*3 Array destructuring */
    // //==========================================
    // const foo = ['one', 'two', 'three'];
    //
    // const [red, yellow, green] = foo;
    // console.log(red); // "one"
    // console.log(yellow); // "two"
    // console.log(green); // "three"




};

export default Destructuring;




