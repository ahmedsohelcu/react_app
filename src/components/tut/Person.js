import React from 'react';


const Person = (props) =>{
    return(
        <div className="col-md-4">
            <div className="card">
                <ul className="list-group list-group-flush">
                    <li className="list-group-item text-left">
                        Id :<code>{props.pr.id}</code>
                    </li>

                    <li className="list-group-item text-left">
                        Name : <code>{props.pr.name}</code>
                    </li>

                    <li className="list-group-item text-left">
                        Company <code>:{props.pr.company}</code>
                    </li>
                </ul>
            </div>
            <br/>
        </div>
    )
};

export default Person;