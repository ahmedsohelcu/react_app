import React,{useState} from 'react'

const Test = ()=>{

    const [state,setState] = useState({
       id:'',
       name:'',
       email:''
    });

    const [lists,setLists] = useState([
        {
            id:'01',
            name:'Ahmed Sohel',
            email:'ahmed@gmail.com'
        },
        {
            id:'02',
            name:'Ahmed ullah',
            email:'ahmed@gmail.com'
        },
        {
            id:'03',
            name:'Jamir uddin',
            email:'Jamir@gmail.com'
        },
        {
            id:'04',
            name:'Kamal Ahmed',
            email:'kamal@gmail.com'
        }

    ]);

    const changeHandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;

          setState({
              ...state,
              [name]:value
          });

        console.log(name+value);
    };



    const storeFormData = (e)=>{
        e.preventDefault();
        let list = {
            id: state.id,
            name: state.name,
            email: state.email,
        };

        setLists(lists.concat([
            list
        ]));

        console.log(e);

    };


    const clickHandler = (e)=>{
        console.log(e)
    }




    return(
        <div className='container'>
            <div className="row">
                <div className="col-md-12 card card-body">
                    <div className="bg-dark text-light p-3">
                        <h2>This is form Test Component...</h2>
                    </div>
                    <div className="content text-justify">
                        <form onSubmit={storeFormData}>
                            <div className="form-group">
                                <label htmlFor="name">Id</label>
                                <input type="text" name="id" onChange={changeHandler} value={state.id} className="form-control" />
                            </div>

                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input type="text" id="name" name="name" onChange={changeHandler} value={state.name} className="form-control" />
                            </div>

                            {state.name}
                            {state.email}

                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="email" id="email" onChange={changeHandler} name={'email'} value={state.email} className="form-control"/>
                            </div>

                            <div className="form-group">
                                <input type="submit" className="btn btn-success" name="submit" value="Store" />
                            </div>
                        </form>
                    </div>


                    <table className="table table-striped">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        {
                            lists.map((list,index)=>{
                                return(
                                    <tr key={index}>
                                        <td>{list.id}</td>
                                        <td key={index}>{list.name}</td>
                                        <td>{list.email}</td>
                                        <td>
                                            <button
                                                    className='btn btn-info btn-sm'>Edit</button> &nbsp;
                                            <button onClick={clickHandler} className='btn btn-danger btn-sm'>Delete</button>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </table>
                </div>
            </div>
            <br/>
        </div>
    );
};


export default Test;

