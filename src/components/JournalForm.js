import React, {useState} from 'react'

const JournalForm = () => {  
    const [state, setState] = useState({
        name: '',
        email: '',
    });

    const [list, setList] = useState([


    ]);

    const submitForm = (e) => {
        e.preventDefault();
        console.log(state);

        let new_value =  [
            {name: state.name}
        ];
        
        setList(list.concat([
           new_value
        ]))
    };

    const handleFromValue = (e) => {
        console.log(e.target.name);
        let name = e.target.name;
        let value = e.target.value;

        setState({
            ...state,
            [name]: value
        })
    };

    return (
        <div>
            <form onSubmit={submitForm}>
                <input type="text" name="name" value={state.name} onChange={handleFromValue} />
                <input type="text" name="email" value={state.email} onChange={handleFromValue} />
                <input type="submit" value="Submit"/>
            </form>
        
            <ul>
                {list.map((item, idx) => 
                    <li key={idx}>
                        Names : {item.name}
                    </li>
                )}
            </ul>

        </div>
    )
};

export default JournalForm;



// import React, {useState} from 'react'

// const JournalForm = () => {
//     const [state, setState] = useState('')

//     const submitForm = (e) => {
//         e.preventDefault()
//         console.log(state)
//     }

//     const handleFromValue = (e) => {
//         console.log(e.target.value)
//         setState(e.target.value)
//     }

//     return (
//         <>
//         <form onSubmit={submitForm}>
//             <input type="text" value={state} onChange={handleFromValue} />
//             <input type="submit" value="Submit" />
//         </form>
//         <p>{state}</p>
//         </>
//     )
// }

// export default JournalForm