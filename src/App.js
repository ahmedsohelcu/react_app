import React,{useState} from 'react';
import './App.css';
import JournalForm from './components/JournalForm'
import MyForm from './components/MyForm'
import Test from './components/Test'
import Destructuring from './components/tut/Destructuring'
import Looping from './components/tut/Looping'
import Persons from './components/tut/Persons'
import FilterTest from './components/tut/FilterTest';
import Clock from './components/tut/Clock';

// import WallClock from './components/tut/WallClock';
import AddTodo from './components/tut/AddTodo';
import ListTodo from './components/tut/ListsTodo';


function App() {
    const persons = [
        {
            "id": 0,
            "name": "Vernon Dunham",
            "company": "Qualcore",
            "email": "vernon.dunham@qualcore.com"
        },
        {
            "id": 1,
            "name": "Dori Neal",
            "company": "Sunopia",
            "email": "dori.neal@sunopia.com"
        },
        {
            "id": 5,
            "name": "Raymond Seabury",
            "company": "Airconix",
            "email": "raymond.seabury@airconix.com"
        }, {
            "id": 6,
            "name": "Dori Pettey",
            "company": "Hivemind",
            "email": "dori.pettey@hivemind.com"
        }, {
            "id": 7,
            "name": "Vernon Neal",
            "company": "Qualcore",
            "email": "vernon.neal@qualcore.com"
        },{
            "id": 9,
            "name": "Vernon Muldoon",
            "company": "Airconix",
            "email": "vernon.muldoon@airconix.com"
        },
        {
            "id": 10,
            "name": "Vernon Seabury",
            "company": "Hivemind",
            "email": "vernon.seabury@hivemind.com"
        },
        {
            "id": 11,
            "name": "Dori Neal",
            "company": "Airconix",
            "email": "dori.neal@airconix.com"
        },
        {
            "id": 12,
            "name": "Raymond Pettey",
            "company": "Airconix",
            "email": "raymond.pettey@airconix.com"
        },
        {
            "id": 13,
            "name": "Rico Muldoon",
            "company": "Qualcore",
            "email": "rico.muldoon@qualcore.com"
        },
        {
            "id": 14,
            "name": "Vernon Seabury",
            "company": "Sunopia",
            "email": "vernon.seabury@sunopia.com"
        },
        {
            "id": 15,
            "name": "Rico Pettey",
            "company": "Airconix",
            "email": "rico.pettey@airconix.com"
        },
        {
            "id": 18,
            "name": "Jason Pettey",
            "company": "Thermolock",
            "email": "jason.pettey@thermolock.com"
        },
        {
            "id": 19,
            "name": "Vernon Dunham",
            "company": "Thermolock",
            "email": "vernon.dunham@thermolock.com"
        }
    ];


    const [todoList,setlists] = useState(
        [
            {
                "id": 1,
                "name": "Ahmed sohel",
                "email": "sohel@gmail.com"
            },
            {
                "id": 2,
                "name": "Ahmed ullah",
                "email": "ahmed@gmail.com"
            },
            {
                "id": 3,
                "name": "Hasanuzzaman",
                "email": "hasan@airconix.com"
            }
        ]
    );

    const edit = (id,e)=>{
        alert('id is '+id);
    };

    ///Delete todolist ....
    const deleteTodo = (id,e)=>{
        let newtodos = todoList.filter(todo => todo.id != id);

        setlists([...newtodos]);
    };

    //===================================start_add_todo_=============================
    const [todo,setTodo] = useState({
        id: '',
        name : '',
        email:''
    });

    const inputHandler = (e)=>{
        e.preventDefault();
        let name = e.target.name;
        let value = e.target.value;

        setTodo({
            ...todo,
            [name]:value
        });
    };

    const formSubmit = (e)=>{
        e.preventDefault();

        let new_todo = {
            id: todoList.length+1,
            name:todo.name,
            email: todo.email
        };

        setlists(todoList.concat(
            [new_todo]
        ));
    };
    //===================================end_add_todo_=============================

  return (
    <div className="App">
        <br/>
        <div className="container">
            <hr/><h2 className={'text-left'}>1. List of Persons <code>(Using Persons and Person Component)</code> </h2><hr/>
            <div className="row">
                <Persons persons={persons}/>
            </div>
        </div>
        <br/>

        <div className="container"><hr/>
            <h2 className={'text-left'}>2.Filter Test <code>(Using FilterTest Component)</code> </h2><hr/>
            <div className="row">
                <FilterTest data={persons} />
            </div>
        </div>
        <hr/>

        <div className="container"><hr/>
            <h2 className={'text-left'}>
                Todos  <code>(Using AddTodo and Lists todo Component)</code>
            </h2><hr/>

            <div className="row">
               <div className="col-md-5">
                   <h2>Add Todo ...</h2>
                   <AddTodo inputHandler={inputHandler} todo={todo} formSubmit={formSubmit} todoLists={todoList}/>
               </div>

                <div className="col-md-7">
                    <h2>Lists Todo ..</h2>
                    <ListTodo edit={edit} deleteTodo={deleteTodo} todoLists={todoList}/>
                </div>
            </div>
        </div><hr/>



        {/*<div className="container"><hr/>*/}
        {/*    <h2 className={'text-left'}>3.Destructuring <code>(Using Destructuring Component)</code> </h2><hr/>*/}
        {/*    <div className="row">*/}
        {/*        <Destructuring/>*/}
        {/*    </div>*/}
        {/*</div>*/}


        <div className="container"><hr/>
            <h2 className={'text-left'}>4.Clock <code>(Using Clock Component)</code> </h2><hr/>
            <div className="row">
                <div id="mount">

                </div>


                <div id="root" className="container">

                </div>

                <Clock/>

                {/*<WallClock size={400} timeFormat="24hour" hourFormat="standard" />*/}

            </div>
        </div>
        <hr/>



        {/* <Test></Test> */}

{/*  */}
      {/* <JournalForm /> */}
      <hr/>

        {/* <MyForm/> */}
    </div>
  );
}

export default App;
